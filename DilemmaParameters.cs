namespace PrisonerDilemma;

// TODO : What does the `readonly` keyword do here?
/// <summary>
/// Parameters for a prisoner's dilemma situation.
/// </summary>
readonly struct DilemmaParameters
{
  /// <summary>
  /// Reward for one prisoner if both cooperate.
  /// </summary>
  public double CooperateReward { get; init; }

  /// <summary>
  /// Reward for one prisoner if only they defect.
  /// </summary>
  public double SnitchReward { get; init; }

  // <summary>
  /// Reward for one prisoner if only they cooperate.
  /// </summary>
  public double CaughtReward { get; init; }

  // <summary>
  /// Reward for one prisoner if both defect.
  /// </summary>
  public double DefectReward { get; init; }
}